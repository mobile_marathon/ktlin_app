import kotlinx.coroutines.*
import kotlin.concurrent.thread
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

fun main() {
    thread(start = true) {
        Thread.sleep(1000)
        println("World")
    }
    Thread.sleep(2000)
    println("Hello,")
    val start = System.currentTimeMillis()

    // Последовательный вызов функций
    val sum1 = runBlocking { delayedSum1() }
    val sum2 = runBlocking { delayedSum2() }
    val result = sum1 + sum2
    println("main: I'm tired of waiting!")
    println("main: I'm running finally")
    println("Сумма: $result")
    println("Время выполнения: ${System.currentTimeMillis() - start} мс")
    println("main: Now I can quit.")

    // Асинхронный вызов функций
    runBlocking {
        val deferredSum1 = this.async { delayedSum1() }
        val deferredSum2 = this.async { delayedSum2() }
        val result = deferredSum1.await() + deferredSum2.await()
        println("main: I'm running finally")
        println("Сумма: $result")
        println("Время выполнения: ${System.currentTimeMillis() - start} мс")
        println("main: Now I can quit.")
    }
}
suspend fun delayedSum1(): Int {
    delay(1000)
    println("I'm sleeping  ...")
    return 2
}
suspend fun delayedSum2(): Int {
    delay(2000)
    println("I'm sleeping  ...")
    return 3
}
package com.example.myapplication

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ScrollView
import android.widget.TextView

class InfoActivity : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        // Получение данных из Intent, переданных с предыдущего экрана
        val firstName = intent.getStringExtra("name")
        val lastName = intent.getStringExtra("patronymic")
        val middleName = intent.getStringExtra("surname")
        val age = intent.getIntExtra("age", 0)
        val hobby = intent.getStringExtra("hobby")

        // Определение View элементов в разметке
        val scrollView: ScrollView = findViewById(R.id.scroll_view)
        val textViewInfo: TextView = findViewById(R.id.text_view_info)

        // Формирование текста, который будет отображаться на экране
        val text = when {
            age < 18 -> {
                "$lastName $firstName $middleName - несовершеннолетний. Ему(ей) $age лет. " +
                        "Занимается: $hobby"
            }
            age < 60 -> {
                "$lastName $firstName $middleName - работоспособный гражданин. Ему(ей) $age лет. " +
                        "Занимается: $hobby"
            }
            else -> {
                "$lastName $firstName $middleName - пенсионер. Ему(ей) $age лет. " +
                        "Занимается: $hobby"
            }
        }

        // Отображение текста на экране
        textViewInfo.text = text
    }
}
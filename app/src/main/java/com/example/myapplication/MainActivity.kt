package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val surnameEditText = findViewById<EditText>(R.id.surnameEditText)
        val nameEditText = findViewById<EditText>(R.id.nameEditText)
        val patronymicEditText = findViewById<EditText>(R.id.patronymicEditText)
        val ageEditText = findViewById<EditText>(R.id.ageEditText)
        val hobbyEditText = findViewById<EditText>(R.id.hobbyEditText)
        val infoButton = findViewById<Button>(R.id.infoButton)

        infoButton.setOnClickListener {
            val surname = surnameEditText.text.toString()
            val name = nameEditText.text.toString()
            val patronymic = patronymicEditText.text.toString()
            val age = ageEditText.text.toString()
            val hobby = hobbyEditText.text.toString()

            val intent = Intent(this, InfoActivity::class.java).apply {
                putExtra("surname", surname)
                putExtra("name", name)
                putExtra("patronymic", patronymic)
                putExtra("age", age)
                putExtra("hobby", hobby)
            }
            startActivity(intent)
        }
    }
}
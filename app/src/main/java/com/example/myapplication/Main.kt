package com.example.myapplication
import android.annotation.SuppressLint
import android.graphics.Color
import android.widget.ImageView
import android.widget.Button
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlin.random.Random

class Main : AppCompatActivity() {

        private lateinit var imageView: ImageView
        private lateinit var button: Button

        private val images = arrayOf(
            R.drawable.image1,
            R.drawable.image2,
            R.drawable.image3,
            R.drawable.image4,
            R.drawable.image5,
            R.drawable.image6,
            R.drawable.image7
        )

        @SuppressLint("MissingInflatedId")
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.main)

            imageView = findViewById(R.id.imageView)
            button = findViewById(R.id.button)

            button.setOnClickListener {
                val randomIndex = Random.nextInt(0, images.size)
                imageView.setBackgroundResource(images[randomIndex])
                val randomColor = Color.rgb(Random.nextInt(256), Random.nextInt(256), Random.nextInt(256))
                button.setBackgroundColor(randomColor)
            }
        }
}